## Install
```
git clone https://bars3s@bitbucket.org/bars3s/order-book-matching-test.git
cd order-book-matching-test
npm install
```

## Run Test
```
npm run test
```
