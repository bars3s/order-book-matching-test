const expect = require('chai').expect;

/**
 * Calculate profit in case matching of the order book
 * @param {Array} asks
 * @param {Array} bids
 * @return {Object}
 */
function orderBookMatchingProfit(asks, bids) {
    const askLength = asks.length;
    const bidLength = bids.length;

    if (!askLength || !bidLength) {
        return {
            amount: 0,
            percent: 0
        };
    }

    let sumBuy = 0;
    let sumSell = 0;
    let sumVolume = 0;

    let askCursor = 0;
    let bidCursor = 0;
    let currentAsk = asks[askCursor];
    let currentBid = bids[bidCursor];
    let currentAskAmount = currentAsk.amount;
    let currentBidAmount = currentBid.amount;

    while (asks[askCursor].rate < bids[bidCursor].rate) {
        const matchedVolume = Math.min(currentAskAmount, currentBidAmount);

        currentAskAmount -= matchedVolume;
        currentBidAmount -= matchedVolume;

        sumVolume += matchedVolume;
        sumBuy += matchedVolume * currentAsk.rate;
        sumSell += matchedVolume * currentBid.rate;

        if (currentAskAmount === 0) {
            askCursor++;
            currentAsk = asks[askCursor];
            currentAskAmount = currentAsk.amount;

            if (askLength <= askCursor) {
                break;
            }
        }

        if (currentBidAmount === 0) {
            bidCursor++;

            if (bidLength <= bidCursor) {
                break;
            }

            currentBid = bids[bidCursor];
            currentBidAmount = currentBid.amount;
        }
    }

    return {
        amount: Math.round(sumVolume * 100) / 100,
        percent: 100 * (sumSell > 0 ? sumSell / sumBuy - 1 : 0)
    };
}

function generateOrderBook(from, to, reverse = false) {
    const orderBook = [];
    let lastPrice = from;

    while (lastPrice < to) {
        orderBook.push({
            rate: lastPrice,
            amount: generateAmount()
        });

        lastPrice = generatePrice(lastPrice);
    }

    // to be sure that "to" price is also included in book
    orderBook.push({
        rate: to,
        amount: generateAmount()
    });

    if (reverse) {
        orderBook.reverse();
    }

    return orderBook;

    function generatePrice(basePrice) {
        return Math.round((basePrice + Math.round(Math.random() * 100) / 100 + 0.01) * 100) / 100;
    }

    function generateAmount() {
        return Math.round(Math.round(Math.random() * 10000) / 100) + 0.01;
    }
}


describe('Test order book matching profile calculations', function () {
    this.timeout(20000);

    function checkExpectation(result, expectPercent, expectAmount) {
        expect(result).to.have.property('percent').to.equal(expectPercent);
        expect(result).to.have.property('amount').to.equal(expectAmount);
    }

    it('no price crossing so it should return zero profit', function () {
        const result = orderBookMatchingProfit(generateOrderBook(90, 100), generateOrderBook(80, 89.999, true));
        checkExpectation(result, 0, 0);
    });

    it('no price crossing, but price equality so it should return zero profit', function () {
        const result = orderBookMatchingProfit(generateOrderBook(90, 100), generateOrderBook(80, 90, true));
        checkExpectation(result, 0, 0);
    });

    it('it has one price crossing so it should return 50% profit with 1 amount volume', function () {
        const askOrderBook = [{amount: 2, rate: 50}, {amount: 1, rate: 100}];
        const bidOrderBook = [{amount: 1, rate: 75}, {amount: 1, rate: 25}];

        const result = orderBookMatchingProfit(askOrderBook, bidOrderBook);

        checkExpectation(result, 50, 1);
    });

    it('it has one price crossing and one price equality so it should return 100% profit with 1 amount volume', function () {
        const askOrderBook = [{amount: 2, rate: 50}, {amount: 1, rate: 150}];
        const bidOrderBook = [{amount: 1, rate: 100}, {amount: 1, rate: 50}];

        const result = orderBookMatchingProfit(askOrderBook, bidOrderBook);

        checkExpectation(result, 100, 1);
    });

    it('it has two price crossing, it should return 62.5% profit with 2 amount volume', function () {
        const askOrderBook = [{amount: 2, rate: 50}, {amount: 1, rate: 60}, {amount: 1, rate: 150}];
        const bidOrderBook = [{amount: 1, rate: 100}, {amount: 2, rate: 80}];

        const result = orderBookMatchingProfit(askOrderBook, bidOrderBook);

        checkExpectation(result, 62.5, 3);
    });

    it(`performance test`, function () {

        const loopTimes = [];
        for (let loopInc = 0; loopInc < 10; loopInc++) {
            console.log(`Start loop: ${loopInc + 1}`);

            const askBook = generateOrderBook(100, 1000000);
            const bidBook = generateOrderBook(1, 1000000, true);

            console.log('ASK length: ', askBook.length);
            console.log('BID length: ', bidBook.length);

            const startTime = process.hrtime();

            const result = orderBookMatchingProfit(askBook, bidBook);

            const stopTime = process.hrtime(startTime);

            console.log('Result: ', result);
            const loopTime = stopTime[0] + stopTime[1] / 1e9;
            console.log(`Loop time: ${loopTime}s`);

            loopTimes.push(loopTime);
        }

        const avgTime = loopTimes.reduce((sum, t) => sum + t, 0) / loopTimes.length;
        console.log('Average time: ' + avgTime + 's');
    });
});